package com.campusacademy.pokedex.repo;

import com.campusacademy.pokedex.model.Pokemon;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PokemonRepo extends JpaRepository<Pokemon, Long> {
}
