package com.campusacademy.pokedex.service;

import java.util.List;

import com.campusacademy.pokedex.model.Pokemon;
import com.campusacademy.pokedex.repo.PokemonRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PokemonService {
    private final PokemonRepo pokemonRepo;

    @Autowired
    public PokemonService(PokemonRepo pokemonRepo) {
        this.pokemonRepo = pokemonRepo;
    }

    public Pokemon addPokemon(Pokemon pokemon) {
        return pokemonRepo.save(pokemon);
    }

    public List<Pokemon> findAllPokemons() {
        return pokemonRepo.findAll();
    }

    public void deletePokemon(Long id) {
        pokemonRepo.deleteById(id);
    }
}
